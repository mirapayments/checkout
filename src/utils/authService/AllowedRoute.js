import React from "react";
import { Route, Redirect } from "react-router-dom";

const AllowedRoute = ({ component: Component, auth, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      auth === true ? (
        <Redirect
          to={{
            pathname: "/pages",
            state: {
              from: props.location,
            },
          }}
        />
      ) : (
        <Component {...props} />
      )
    }
  />
);

export default AllowedRoute;
