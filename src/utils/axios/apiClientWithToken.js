import axios from "axios";

const baseUrl =
  process.env.NODE_ENV !== "production"
    ? "http://localhost:8000"
    : "https://api.mirapayments.com";

const axiosClient = axios.create({
  baseURL: process.env.REACT_APP_TEST_WITH_LIVE_SERVER === "yes" ? "https://api.mirapayments.com" : baseUrl,
  headers: {
    "Content-Type": "application/json",
  },
});

axiosClient.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    // return error.response
    let res = error.response;
    // redirect rules for all 401 responses to another page
    if (res.status >= 401 || res.status < 500) {
      return res;
    }
    // return error
    // return res
    return Promise.reject(error);
  }
);

axiosClient.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem("token");
    if (token) {
      config.headers.authorization = `Token ${token}`;
    }
    return config;
  },
  (error) => Promise.reject(error)
);

export { axiosClient };
