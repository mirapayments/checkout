import { axiosClient } from "../axios/apiClientWithToken";

export function getAccountCredentials(account_number) {
  return axiosClient.get(`/accounts/credentials/${account_number}/`);
}
