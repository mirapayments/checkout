import { axiosClient } from "../axios/apiClient";

export function createTransaction(account_number, data) {
  return axiosClient.post(`/transactions/charge/${account_number}/`, data);
}