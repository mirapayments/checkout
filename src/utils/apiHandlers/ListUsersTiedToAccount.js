import { axiosClient } from "../axios/apiClientWithToken";

export function listUsersTiedToAccount(account_number) {
  return axiosClient.get(`/users/list/${account_number}/`);
}
