import { axiosClient } from "../axios/apiClient";

export async function getCheckoutDetails(code) {
  return axiosClient.get(`/payments/checkout/code/${code}/`);
}
