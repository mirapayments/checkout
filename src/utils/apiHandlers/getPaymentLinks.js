import { axiosClient } from "../axios/apiClient";

export async function getPaymentLinks(code) {
  return axiosClient.get(`/payments/payment-link/code/${code}/`);
}
