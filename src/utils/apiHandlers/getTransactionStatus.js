import { axiosClient } from "../axios/apiClientWithToken";

export function getTransactionStatus(reference) {
  return axiosClient.get(`/transactions/status/${reference}/`);
}
