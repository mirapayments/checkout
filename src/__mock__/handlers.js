import { rest } from "msw";
import payResponse from "../fixtures/pay.json";

const base =
  process.env.NODE_ENV !== "production"
    ? "http://localhost:8000"
    : "https://api.mirapayments.com";

const baseUrl =
  process.env.REACT_APP_TEST_WITH_LIVE_SERVER == "yes"
    ? "https://api.mirapayments.com"
    : base;

export const handlers = [
  rest.get(`${baseUrl}/payments/checkout/code/undefined/`, (req, res, ctx) => {
    return res(ctx.json(payResponse));
  }),
];
