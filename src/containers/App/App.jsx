import React, { Fragment } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import '../../scss/app.scss';
import Router from './Router';
import store from '../../redux/store/store';
import ScrollToTop from './ScrollToTop';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';

const App = () => {
  // const [isLoaded, setIsLoaded] = useState(false);

  let persistor = persistStore(store);

  // useEffect(() => {
  //   window.addEventListener('load', () => {
  //     // setIsLoading(false);
  //     // setIsLoaded(true)
  //     setTimeout(() => setIsLoaded(true), 2000);
  //   });
  //   // return () => window.removeEventListener("load", () => setIsLoaded(true));
  // });

  // //didn't add a render dependency might lead to stack overflow still watching

  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <BrowserRouter>
          <ScrollToTop>
            <Fragment>
              <Router />
            </Fragment>
          </ScrollToTop>
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
};

export default App;
