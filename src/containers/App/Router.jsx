import React from "react";
import { Route, Switch } from "react-router-dom";
import MainWrapper from "./MainWrapper";
import Home from "../Home/Home";
import NotFound from "../DefaultPage/404/index";
import Checkout from "../checkout/index";
import Pay from "../Pay/index";
import TransactionStatusPage from "../TransactionStatusPage/TransactionStatusPage";

const Router = () => {
  return (
    <MainWrapper>
      <main>
        <Switch>
          <Route exact path="/404" component={NotFound} />
          <Route exact path="/" component={Home} />
          <Route exact path="/checkout" component={Checkout} />
          <Route exact path="/pay/:code" component={Pay} />
          <Route exact path="/transaction/status" component={TransactionStatusPage} />
          <Route component={NotFound} />
        </Switch>
      </main>
    </MainWrapper>
  );
};

export default Router;
