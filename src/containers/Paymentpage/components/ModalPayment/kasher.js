(function () {
    var openFrameDiv, openFrameBtn, kashierIframe, paymentObj = {};
    var initializer = {
        insertCSS: function () {
            var css = ".hide{display: none !important;; }.show{display: block !important;;} .el-kashier-button{ border:none; border-radius: 3px !important; background-color: #19bdc3 !important;color: #fff !important;font-size: 16px !important;height: 40px;overflow: hidden;display: inline-block;visibility: visible!important;text-decoration: none;box-shadow: none !important;cursor: pointer;padding: 6px 43px !important;  }.el-kashier-button span{background: none !important;box-shadow: none !important;height: inherit;line-height: 30px;font-size: 16px;color: #fff;font-weight: 700;}.el-kashier-button.active,.el-kashier-button:not(:disabled):active{background:#005d93}.el-kashier-button.active span,.el-kashier-button:not(:disabled):active span{color:#eee;background:#008cdd;background-image:-webkit-linear-gradient(#008cdd,#008cdd 85%,#239adf);background-image:-moz-linear-gradient(#008cdd,#008cdd 85%,#239adf);background-image:-ms-linear-gradient(#008cdd,#008cdd 85%,#239adf);background-image:-o-linear-gradient(#008cdd,#008cdd 85%,#239adf);background-image:linear-gradient(#008cdd,#008cdd 85%,#239adf);-webkit-box-shadow:inset 0 1px 0 rgba(0,0,0,.1);-moz-box-shadow:inset 0 1px 0 rgba(0,0,0,.1);-ms-box-shadow:inset 0 1px 0 rgba(0,0,0,.1);-o-box-shadow:inset 0 1px 0 rgba(0,0,0,.1);box-shadow:inset 0 1px 0 rgba(0,0,0,.1)}.el-kashier-button.disabled,.el-kashier-button:disabled{background:rgba(0,0,0,.2);-webkit-box-shadow:none;-moz-box-shadow:none;-ms-box-shadow:none;-o-box-shadow:none;box-shadow:none}.el-kashier-button.disabled span,.el-kashier-button:disabled span{color:#999;background:#f8f9fa;text-shadow:0 1px 0 rgba(255,255,255,.5)}";
            var node = document.createElement('style');
            node.innerHTML = css;
            document.body.appendChild(node);
        },
        insertAfter: function (el, referenceNode) {
            referenceNode.parentNode.insertBefore(el, referenceNode.nextSibling);
        },
        assigningDataAttributes: function () {
            paymentObj.checkoutAmount = document.querySelector('#kashier-iFrame').getAttribute('data-amount');
            paymentObj.checkoutDescription = document.querySelector('#kashier-iFrame').getAttribute('data-description');
            paymentObj.currency = document.querySelector('#kashier-iFrame').getAttribute('data-currency');
            paymentObj.orderId = document.querySelector('#kashier-iFrame').getAttribute('data-orderId');
            paymentObj.cardHolderName = document.querySelector('#kashier-iFrame').getAttribute('data-cardHolderName');
            paymentObj.expiryDate = document.querySelector('#kashier-iFrame').getAttribute('data-expiryDate');
            paymentObj.merchantRedirect = document.querySelector('#kashier-iFrame').getAttribute('data-merchantRedirect');
            paymentObj.merchantId = document.querySelector('#kashier-iFrame').getAttribute('data-merchantId');
            paymentObj.hash = document.querySelector('#kashier-iFrame').getAttribute('data-hash');
            paymentObj.storeName = document.querySelector('#kashier-iFrame').getAttribute('data-store');
            paymentObj.type = document.querySelector('#kashier-iFrame').getAttribute('data-type');
            paymentObj.paymentRequestId = document.querySelector('#kashier-iFrame').getAttribute('data-paymentRequestId');
            paymentObj.display = document.querySelector('#kashier-iFrame').getAttribute('data-display');
            paymentObj.metaData = document.querySelector('#kashier-iFrame').getAttribute('data-metaData');
            paymentObj.allowedMethods = document.querySelector('#kashier-iFrame').getAttribute('data-allowedMethods');
            paymentObj.defaultMethod = document.querySelector('#kashier-iFrame').getAttribute('data-defaultMethod');
            paymentObj.url = document.querySelector('#kashier-iFrame').getAttribute('src').replace(/(\/\/.*?\/).*/g, '$1');
            paymentObj.brandColor = document.querySelector('#kashier-iFrame').getAttribute('data-brandColor');
            paymentObj.mode = document.querySelector('#kashier-iFrame').getAttribute('data-mode');
            paymentObj.styleUrl = document.querySelector('#kashier-iFrame').getAttribute('data-styleUrl');
            paymentObj.redirectMethod = document.querySelector('#kashier-iFrame').getAttribute('data-redirectMethod');
            paymentObj.failureRedirect = document.querySelector('#kashier-iFrame').getAttribute('data-failureRedirect');
            var backgroundColor = document.querySelector('#kashier-iFrame').getAttribute('data-iframeBackgroundColor');
            if (new RegExp('^#(?:[0-9a-fA-F]{3}){1,2}$').test(backgroundColor)) {
                backgroundColor = backgroundColor.substring(1);
                paymentObj.iframeBackgroundColor = backgroundColor;
            }
        },
        buttonBuilder: function () {
            openFrameDiv = document.createElement("DIV");
            openFrameDiv.classList.add("el-kashier-div-button");
            openFrameBtn = document.createElement("BUTTON");
            openFrameDiv.appendChild(openFrameBtn);
            var paymentNowKey = (paymentObj.display == 'ar') ? "Ø§Ø¯ÙØ¹ Ø§Ù„Ø§Ù†" : "Pay Now";
            var btnText = '<span id="paymentLoader" class="hide">' +
                ' <i class="fa fa-circle-o-notch fa-spin"></i></span>' +
                '<span id="paymentAmount" class="show">' + paymentNowKey + '</span></span>'
            openFrameBtn.classList.add("el-kashier-button");
            openFrameBtn.setAttribute("id", "el-kashier-button");
            initializer.insertAfter(openFrameDiv, document.querySelector("#kashier-iFrame"));
            openFrameBtn.innerHTML = btnText;
            initializer.insertCSS();                              // Append the text to <button>
            parent.postMessage({ message: "iframeLoaded", params: {} }, '*');
        },
        iFrameBuilder: function (paymentObj) {
            var sourceUrl = `${paymentObj.url}?mode=${paymentObj.mode}&paymentType=iframe&amount=${paymentObj.checkoutAmount}&currency=${paymentObj.currency}&order=${paymentObj.orderId}&merchantId=${paymentObj.merchantId}&merchantRedirect=${paymentObj.merchantRedirect}&display=${paymentObj.display}&hash=${paymentObj.hash}&store=${paymentObj.storeName}&type=${paymentObj.type}&paymentRequestId=${paymentObj.paymentRequestId}&allowedMethods=${paymentObj.allowedMethods}&redirectMethod=${paymentObj.redirectMethod}&iframeBackgroundColor=${paymentObj.iframeBackgroundColor}&metaData=${paymentObj.metaData}&failureRedirect=${paymentObj.failureRedirect}&brandColor=${paymentObj.brandColor?encodeURIComponent(paymentObj.brandColor):''}&defaultMethod=${paymentObj.defaultMethod}`;
            kashierIframe = document.createElement("iframe");
            kashierIframe.src = sourceUrl;
            kashierIframe.id = 'iFrame';
            kashierIframe.style = 'z-index: 2147483647; \
            display: block;\
            border: 0px none transparent;\
            overflow-x: hidden;\
            overflow-y: auto;\
            overflow: scroll;\
            visibility: visible;\
            margin: 0px;\
            padding: 0px;\
            -webkit-tap-highlight-color: transparent;\
            position: fixed;\
            left: 0px;\
            top: 0px;\
            width: 100%;\
            height: 100%;\
            min-height: 100%;'
            kashierIframe.frameborder = 'no';
        },
        messageListener: function listenerFunction(e) {
            loadingSpinner()
            var iFrameMessage = e.data;
            switch (iFrameMessage.message) {
                case "closeIframe":
                    if (document.getElementById('iFrame'))
                        document.getElementById('iFrame').remove()
                    parent.postMessage({ message: "iframeHide", params: {} }, '*');
                    break;
                case "paymentSuccess":
                    parent.postMessage({ message: "paymentSuccess", params: {} }, '*');
                    break;
                case "contentLoaded":
                    loadingSpinner();
                    break;
                case "urlRedirection":
                    if (iFrameMessage.params.redirectMethod && (iFrameMessage.params.redirectMethod).toLowerCase() == 'post') {
                        const url = new URL(iFrameMessage.params.redirectUrl);
                        const port = url.port;
                        if (port) {
                            fullRedirectUrl = url.protocol + '//' + url.hostname + ':' + port + url.pathname, parseQuery(url.search)
                        } else {
                            fullRedirectUrl = url.protocol + '//' + url.hostname + url.pathname, parseQuery(url.search)
                        }
                        return redirectPost(fullRedirectUrl, url.search)
                    }
                    var parsedUrl = iFrameMessage.params.redirectUrl.replace(/&amp;/g, "&");
                    window.location.replace(encodeURI(parsedUrl));
                    break;
                default:
                    break;
            }
        }
    }
    initializer.assigningDataAttributes();
    initializer.iFrameBuilder(paymentObj);
    initializer.buttonBuilder();
    openFrameBtn.onclick = function () {
        document.getElementById("paymentLoader").className = "show";
        document.getElementById("paymentAmount").className = "hide";
        document.body.appendChild(kashierIframe);
    }
    if (window.addEventListener) {
        addEventListener("message", initializer.messageListener, false)
    } else {
        attachEvent("onmessage", initializer.messageListener)
    }
    function loadingSpinner() {
        document.getElementById("paymentLoader").className = "hide";
        document.getElementById("paymentAmount").className = "show";
    }
    function redirectPost(url, queryString) {
        var form = document.createElement('form');
        var data = Object.fromEntries(new URLSearchParams(queryString));
        document.body.appendChild(form);
        form.method = 'post';
        form.action = url;
        form.name = 'kashier-postpay-form';
        for (var name in data) {
            var input = document.createElement('input');
            input.type = 'hidden';
            input.name = name;
            input.value = data[name];
            form.appendChild(input);
        }
        form.submit();
    }
    function parseQuery(queryString) {
        var query = {};
        var pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
        for (var i = 0; i < pairs.length; i++) {
            var pair = pairs[i].split('=');
            query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
        }
        return query;
    }
})();