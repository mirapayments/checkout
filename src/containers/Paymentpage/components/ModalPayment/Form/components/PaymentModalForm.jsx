
// Import the necessary libraries and methods
import React,{useState} from "react";
import { Nav, NavItem, NavLink, TabContent, TabPane, Col, Container, Row} from "reactstrap";
import classnames from "classnames";
import {setModalTabPosition} from "../../../../../../redux/features/user/uiSlice";
import Card from "./Options/Card/Cardalt"
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import styles from "./PaymentModalForm.module.css";
import { useHistory } from "react-router";


const PaymentTab = ({setModal}) => {
  const dispatch = useDispatch()
  const [tabPosition, setTabPosition] = useState("1")
  // const {modalTabPosition} = useSelector((state) => state.ui)

  console.log("TAB",tabPosition)
  const toggle = (tab) => {
    // if (modalTabPosition !== tab) dispatch(setModalTabPosition(tab))
    if(tabPosition !== tab) setTabPosition(tab)
  };



  return (
      <div>
        <Row>
        
          <Col className={styles.tab_switches} md={3}>
          <div className={styles.navigate}>
          <button onClick={(e)=>{
            e.preventDefault()
            setModal()
            }}>
             Back
          </button>
        </div>
            <div className="tabs__wrap">
              <Nav vertical>
                <NavItem className={styles.tab}>
                  <NavLink
                      className={classnames({ active: tabPosition === "1" })}
                      onClick={() => toggle("1")}
                      style={{cursor: "pointer"}}
                  >
                    Card 
                  </NavLink>
                  <hr/>
                </NavItem>
                <NavItem className={styles.tab}>
                  <NavLink
                      className={classnames({ active: tabPosition === "2" })}
                      onClick={() => toggle("2")}
                      style={{cursor: "pointer"}}
                  >
                    USSD
                  </NavLink>
                  <hr/>
                </NavItem>
                <NavItem className={styles.tab}>
                  <NavLink
                      className={classnames({ active: tabPosition === "3" })}
                      onClick={() => toggle("3")}
                      style={{cursor: "pointer"}}
                  >
                    Bank
                  </NavLink>
                  <hr/>
                </NavItem>
                <NavItem className={styles.tab}>
                  <NavLink
                      className={classnames({ active: tabPosition === "4" })}
                      onClick={() => toggle("4")}
                      style={{cursor: "pointer"}}
                  >
                    Transfer
                  </NavLink>
                  <hr/>
                </NavItem>
                <NavItem className={styles.tab}>
                  <NavLink
                      className={classnames({ active: tabPosition === "5" })}
                      onClick={() => toggle("5")}
                      style={{cursor: "pointer"}}
                  >
                    Account
                  </NavLink>
                  <hr/>
                </NavItem>
              </Nav>
            </div>
          </Col>

          <Col className={styles.option_pages} md={9}>
            <TabContent activeTab={tabPosition}>
              <TabPane tabId="1">
                <Card/>
              </TabPane>
              <TabPane tabId="2">
              <h1>Ussd</h1>
              </TabPane>
              <TabPane tabId="3">
              <h1>Bank</h1>
              </TabPane>
              <TabPane tabId="4">
              <h1>Hospital</h1>
              </TabPane>
              <TabPane tabId="5">
              <h1>Account</h1>
              </TabPane>
            </TabContent>
          </Col>
        </Row>
      </div>

  );
};

export default PaymentTab;









// import React from "react";
// import { Formik } from "formik";
// import * as Yup from "yup";
// import styles from "./PaymentModalForm.module.css";
// import AccountBalanceWallet from "mdi-react/AccountBalanceWalletIcon";
// import Public from "mdi-react/PublicIcon";
// import AttachMoney from "mdi-react/AttachMoneyIcon";
// import { useSelector, useDispatch } from "react-redux";
// import Spinner from "react-spinner-material";
// import Swal from "sweetalert2";

// import { createNewBankAccount } from "../../../../../../utils/apiHandlers/createNewBankAccount";
// import { listUserAccounts } from "../../../../../../utils/apiHandlers/listUserAccounts";
// import { setUserAccounts } from "../../../../../../redux/features/user/userSlice";
// import useOnlineStatus from "../../../../../../utils/hooks/useOnlineStatus";

// const CreateAccountForm = ({ setModal }) => {
//   //theme hack for custom select styles
//   const { className } = useSelector((state) => state.theme);
//   const online = useOnlineStatus();
//   const dispatch = useDispatch();
//   async function createUserBankAccount(data, setSubmitting) {
//     if (!online) {
//       Swal.fire({
//         title: "",
//         text: "You are offline, please check your internet connection.",
//         icon: "error",
//         showConfirmButton: false,
//         timer: 3000,
//       });
//       return;
//     }
//     try {
//       const res = await createNewBankAccount(JSON.stringify(data));
//       if (res.data.status) {
//         Swal.fire({
//           title: "",
//           text: res.data.detail,
//           icon: "success",
//           showConfirmButton: false,
//           timer: 3000,
//         });
//         const accountList = await listUserAccounts();
//         const newUserAccounts = accountList.data.data;

//         dispatch(setUserAccounts(newUserAccounts));
//         setSubmitting(false);
//         setModal(false);
//       }
//     } catch (error) {
//       setSubmitting(false);
//     }
//   }

//   return (
//     <Formik
//       initialValues={{ name: "", balance_currency: "", account_type: "" }}
//       onSubmit={(values, { setSubmitting }) => {
//         setSubmitting(true);
//         createUserBankAccount(values, setSubmitting);
//       }}
//       validationSchema={Yup.object().shape({
//         balance_currency: Yup.string().required("Please select a currency"),
//         name: Yup.string()
//           .required("Please provide an account name.")
//           .min(4, "Account name is too short."),
//         account_type: Yup.string().required("Please select an account type"),
//       })}
//     >
//       {(props) => {
//         const {
//           values,
//           touched,
//           errors,
//           isSubmitting,
//           handleChange,
//           handleBlur,
//           handleSubmit,
//         } = props;
//         return (
//           <form
//             style={{
//               height: "10%",
//             }}
//             className="form"
//             onSubmit={handleSubmit}
//           >
//             <div className={`form__form-group`}>
//               <span className="form__form-group-label">Name</span>
//               <div
//                 className={`form__form-group-field ${
//                   errors.name && "form__form-validation"
//                 }`}
//               >
//                 <div className="form__form-group-icon">
//                   <AccountBalanceWallet />
//                 </div>
//                 <input
//                   name="name"
//                   component="input"
//                   placeholder="Account Name"
//                   onChange={handleChange}
//                   onBlur={handleBlur}
//                   value={values.name}
//                 />
//               </div>
//               <p className="error_validation_text">
//                 {errors.name && touched.name && errors.name}
//               </p>
//             </div>
//             <div className="form__form-group">
//               <span className="form__form-group-label">Currency</span>
//               <div
//                 className={`${styles.select_form_custom_container} ${
//                   errors.balance_currency && "form__form-validation"
//                 }`}
//               >
//                 <div
//                   className={`form__form-group-icon ${styles.select_form_custom_icon}`}
//                 >
//                   <AttachMoney />
//                 </div>
//                 <select
//                   className={
//                     className === "theme-light"
//                       ? styles.select_form_custom_light
//                       : styles.select_form_custom_dark
//                   }
//                   name="balance_currency"
//                   component="select"
//                   type="select"
//                   placeholder="Currency"
//                   onChange={handleChange}
//                   onBlur={handleBlur}
//                   value={values.balance_currency}
//                 >
//                   <option label={"Select a currency"} />
//                   <option value={"NGN"} label={"NGN"} />
//                   <option value={"USD"} label={"USD"} />
//                   <option value={"EUR"} label={"EUR"} />
//                 </select>
//               </div>
//               <p className="error_validation_text">
//                 {errors.balance_currency &&
//                   touched.balance_currency &&
//                   errors.balance_currency}
//               </p>
//             </div>
//             <div className="form__form-group">
//               <span className="form__form-group-label">Account Type</span>
//               <div
//                 className={`${styles.select_form_custom_container} ${
//                   errors.account_type && "form__form-validation"
//                 }`}
//               >
//                 <div
//                   className={`form__form-group-icon ${styles.select_form_custom_icon}`}
//                 >
//                   <Public />
//                 </div>
//                 <select
//                   className={
//                     className === "theme-light"
//                       ? styles.select_form_custom_light
//                       : styles.select_form_custom_dark
//                   }
//                   name="account_type"
//                   component="select"
//                   type="select"
//                   placeholder="Account Type"
//                   onChange={handleChange}
//                   onBlur={handleBlur}
//                   value={values.account_type}
//                 >
//                   <option label={"Select an account type"} />
//                   <option value={"Individual"} label={"Individual"} />
//                   <option value={"Company"} label={"Company"} />
//                   <option value={"Religious"} label={"Religious"} />
//                   <option value={"Government"} label={"Government"} />
//                   <option value={"NGO"} label={"NGO"} />
//                 </select>
//               </div>
//               <p className="error_validation_text">
//                 {errors.account_type &&
//                   touched.account_type &&
//                   errors.account_type}
//               </p>
//             </div>
//             <button
//               type="submit"
//               disabled={isSubmitting}
//               className="btn btn-primary account__btn account__btn--small"
//             >
//               {isSubmitting ? (
//                 <div className={styles.button_elem}>
//                   <Spinner
//                     radius={20}
//                     color={"#333"}
//                     stroke={2}
//                     visible={true}
//                   />
//                 </div>
//               ) : (
//                 "Create Account"
//               )}
//             </button>
//           </form>
//         );
//       }}
//     </Formik>
//   );
// };

// export default CreateAccountForm;
