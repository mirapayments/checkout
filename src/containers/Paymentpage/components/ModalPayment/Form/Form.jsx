import React from "react";
import PaymentModalForm from "./components/PaymentModalForm";

const FormWrapper = ({ setModalOpen }) => (
  <div
    className="account"
    style={{
      minHeight: "0",
      height: "100%",
    }}
  >
    <div className="account__card">
      <div className="account__head">
        <h4 className="account__subhead subhead">Payment Options</h4>
        <p>Please click on your preferred payment option.</p>
      </div>
      <PaymentModalForm setModal={setModalOpen} />
    </div>
  </div>
);

export default FormWrapper;

// if you want to add select, date-picker and time-picker in your app you need to uncomment the first
// four lines in /scss/components/form.scss to add styles
