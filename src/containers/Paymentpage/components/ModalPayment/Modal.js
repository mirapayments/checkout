import React from "react";
import { Modal } from "reactstrap";
import classNames from "classnames";

import Form from "./Form/Form";
import { useSelector } from "react-redux";

// Then inside the component body

const ModalComponent = ({ color, colored, header, modalOpen, toggle }) => {
  const { className } = useSelector((state) => state.theme);

  // const handleSubmit = () => {

  // }

  const modalClass = classNames({
    "modal-dialog--colored": colored,
    "modal-dialog--header": header,
  });

  return (
    <div>
      <div
        styles={{
          display: "flex",
          justifyContent: "flex-end",
        }}
      >
        {/* <button
          color={color}
          onClick={toggle}
          type="submit"
          className={`${styles.button_newaccount} btn btn-primary account__btn account__btn--small`}
        >
  
         Pay
        </button> */}
      </div>
      <Modal
        isOpen={modalOpen}
        toggle={toggle}
        // className={`modal-dialog--${color} ${modalClass}`}
        modalClassName={className}
        wrapClassName={className}
        backdropClassName={className}
        contentClassName={className}
      >
        <div className="modal__body">
          <Form setModalOpen={toggle} />
        </div>
      </Modal>
    </div>
  );
};

export default ModalComponent;
