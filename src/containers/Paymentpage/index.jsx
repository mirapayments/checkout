import React from "react";
import Payment from "./components/Paymentpage";
import styles from "./payment.module.css";
import MirapayLogo from "../../shared/img/mirapaylogo.png";
import {useSelector} from "react-redux"

const Paymentpage = () => {
const {paymentLink} = useSelector((state) => state.ui)
 
 // add description 
 // add title
 
 return <div className="account">
    <div className={`${styles.wrapper} account__wrapper`}>
        <img alt="Company logo" className={styles.logo} src={MirapayLogo}/>{}
        {console.log(paymentLink)}
        <h4 className={styles.description_text}>{paymentLink?.name}</h4>
        <h5 className={`${styles.description_text} ${styles.muted_text} `}>BY {paymentLink?.account_detail?.account_name?.toUpperCase()}</h5>
        <h5 className={`${styles.description_text} mb-4 mt-4`}>{paymentLink?.description}</h5>
      <div className={`${styles.container} account__card`}>
        {/* <div className="account__head">
          <h4 className="account__subhead subhead">Please verify your email address to continue</h4>
        </div> */}
        <Payment />
      </div>
    </div>
  </div>
};

export default Paymentpage;
