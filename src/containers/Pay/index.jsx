// Import the necessary libraries and modules.
import React, { useState, useEffect } from "react";
import PayForm from "./payForm";
import { useParams } from "react-router-dom";
import { getCheckoutDetails } from "../../utils/apiHandlers/getCheckoutDetails";

// Checkout Form
const Pay = () => {
  const { code } = useParams();
  const [checkoutDetails, setCheckoutDetails] = useState(null);
  const [cardList, setCardList] = useState([]);

  async function getCheckoutDetailsHandler() {
    try {
      const response = await getCheckoutDetails(code).catch((error) =>
        console.log(error)
      );
      if (response.status === 200) {
        setCheckoutDetails(response.data.data);
        console.log(checkoutDetails);
      }
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(
    () => {
      getCheckoutDetailsHandler();
    },
    // eslint-disable-next-line
    []
  );

  return checkoutDetails !== null ? (
    <PayForm
      checkoutDetails={checkoutDetails}
      setCardList={setCardList}
      cardList={cardList}
    />
  ) : null;
};

// Export component
export default Pay;
