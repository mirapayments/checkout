// Import the necessary libraries and modules.
import React, { useState, useEffect, useMemo } from "react";
import { useLocation } from "react-router-dom";
import "./TransactionStatusPage.css";
import Success from "./gif/successful.gif";

const TransactionStatusPage = () => {
  const [transactionStatus] = useState("successful");

  function useSearchQuery() {
    const { search } = useLocation();
    return useMemo(() => new URLSearchParams(search), [search]);
  }

  let searchQuery = useSearchQuery();
  let redirect = searchQuery?.get("redirect");
  let url = redirect
    ? `${atob(searchQuery?.get("redirect"))}?reference=${atob(
        searchQuery?.get("reference")
      )}`
    : null;

  useEffect(
    () => {
      if (url) {
        setTimeout(() => (window.location.href = url), 2000);
      }
    },
    // eslint-disable-next-line
    []
  );

  return transactionStatus === "successful" ? (
    <>
      <div className="main-container">
        <div className="content-container">
          <p>Your transaction was {transactionStatus}</p>
          <br />
          <img className="success" src={Success} alt="success" />
        </div>
      </div>
    </>
  ) : (
    <>
      <div className="main-container">
        <div className="content-container">
          <p>Your transaction {transactionStatus}</p>
          <br />
        </div>
      </div>
    </>
  );
};

// Export the component
export default TransactionStatusPage;
