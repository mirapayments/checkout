import React, { useReducer, useState } from 'react';
import styles from './checkout.module.css';
import companyLogo from '../../shared//img/mirapaylogo.png';
import padlock from '../../shared//img/padlock.svg';
import Select from 'react-select';
import MaskedInput from 'react-text-mask';
import { AMERICANEXPRESS, OTHERCARDS, EXPIRYDATE, CVV, CARDARR, CARDICON } from './constant';
import {
  cardNumberValidation,
  cardExpiryValidation,
  textWithSpacesOnly,
  minLength,
} from './validations';

// Reducer Function
const reducer = (state, action) => {
  switch (action.type) {
    case 'card':
      return { ...state, card: action.data };
    case 'expiry':
      return { ...state, expiry: action.data };
    case 'cvv':
      return { ...state, cvv: action.data };
    case 'cardHolder':
      return { ...state, cardHolder: action.data };
    case 'cleanState':
      return { ...action.data };
    default:
      return state;
  }
};

const countriesOptions = [
    { value: 'Nigeria', label: 'Nigeria' },
    { value: 'Ghana', label: 'Ghana' },
    { value: 'Togo', label: 'Togo' },
    { value: 'South Africa', label: 'South Africa' },
    { value: 'Cameroun', label: 'Cameroun' },
    { value: 'Egypt', label: 'Egypt' },
    { value: 'Sudan', label: 'Sudan' },
    { value: 'Kenya', label: 'Kenya' },
  ];

function findDebitCardType(cardNumber) {
  const regexPattern = {
    MASTERCARD: /^5[1-5][0-9]{1,}|^2[2-7][0-9]{1,}$/,
    VISA: /^4[0-9]{2,}$/,
    AMERICAN_EXPRESS: /^3[47][0-9]{5,}$/,
    DISCOVER: /^6(?:011|5[0-9]{2})[0-9]{3,}$/,
    DINERS_CLUB: /^3(?:0[0-5]|[68][0-9])[0-9]{4,}$/,
    JCB: /^(?:2131|1800|35[0-9]{3})[0-9]{3,}$/,
  };

  for (const card in regexPattern) {
    if (cardNumber.replace(/[^\d]/g, '').match(regexPattern[card])) return card;
  }
  return '';
}

// Form
const initialState = { card: '', expiry: '', cvv: '', cardHolder: '' };
const CheckoutForm = (props) => {
  const [error, setError] = useState({});
  const [email, setEmail] = useState('');
  const [cardType, setCardType] = useState();
  const [state, dispatch] = useReducer(reducer, initialState);

  const handleValidations = (type, value) => {
    let errorText;
    switch (type) {
      case 'card':
        setCardType(findDebitCardType(value));
        errorText = cardNumberValidation(value);
        setError({ ...error, cardError: errorText });
        break;
      case 'cardHolder':
        errorText = value === '' ? 'Required' : textWithSpacesOnly(value);
        setError({ ...error, cardHolderError: errorText });
        break;
      case 'expiry':
        errorText = value === '' ? 'Required' : cardExpiryValidation(value);
        setError({ ...error, expiryError: errorText });
        break;
      case 'cvv':
        errorText = value === '' ? 'Required' : minLength(3)(value);
        setError({ ...error, cvvError: errorText });
        break;
      default:
        break;
    }
  };

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handleInputData = (e) => {
    dispatch({ type: e.target.name, data: e.target.value });
  };

  const handleBlur = (e) => {
    handleValidations(e.target.name, e.target.value);
  };

  const checkErrorBeforeSubmit = () => {
    let errorValue = {};
    let isError = false;
    Object.keys(state).forEach(async (val) => {
      if (state[val] === '') {
        errorValue = { ...errorValue, [`${val + 'Error'}`]: 'Required' };
        isError = true;
      }
    });
    setError(errorValue);
    return isError;
  };

  const cleanData = { card: '', expiry: '', cvv: '', cardHolder: '' };
  const handleSubmit = (e) => {
    let errorCheck = checkErrorBeforeSubmit();
    if (!errorCheck) {
      props.setCardList([...props.cardList, { ...state, cardType }]);
      dispatch({
        type: 'cleanState',
        data: cleanData,
      });
      setCardType('');
    }
  };

  return (
    <section className={styles.body}>
      <div className='shadow-sm px-5 pb-3 rounded-3'>
        <img src={companyLogo} alt="Company Logo" className={styles.company_logo} />
        <form className="form d-block" id="paymentForm">
          <div className="mb-3">
            <label className="form__form-group-label" aria-required="true" htmlFor="email-address">
              Email
            </label>
            <input
              type="email"
              className="form__form-group-field "
              id="email-address"
              name="email"
              value={email}
              onChange={handleEmailChange}
              required
              placeholder="Email Address"
            />
          </div>
          <div className="mt-3">
            <label className="form__form-group-label" htmlFor="card">
              Card Information
            </label>
            <MaskedInput
              guide={false}
              mask={
                ['37', '34'].includes(state && state.card.split('').splice(0, 2).join(''))
                  ? AMERICANEXPRESS
                  : OTHERCARDS
              }
              className="form-control"
              id="card"
              name="card"
              value={state.card}
              onChange={handleInputData}
              onBlur={handleBlur}
              placeholderChar={'\u2000'}
              required
              placeholder="5554-5678-3452-7845"
            />
            {(!error || !error.cardError) && CARDARR.includes(cardType) && (
              <img
                style={{ float: 'right', position: 'relative', top: '-35px' }}
                src={CARDICON[cardType]}
                alt="Card"
                width="50px"
                height="33px"
              />
            )}
            {error && error.cardError && error.cardError.length > 1 && <span className='form__form-group-error'>{error.cardError}</span>}
          </div>
          <div className='my-3'>
            <div className="input-group">
              <MaskedInput
                mask={EXPIRYDATE}
                guide={false}
                required
                name="expiry"
                id="expiry"
                className='form-control'
                value={state.expiry}
                placeholder="MM/YY"
                placeholderChar={'\u2000'}
                onChange={handleInputData}
                onBlur={handleBlur}
              />
              
              <MaskedInput
                mask={CVV}
                guide={false}
                name="cvv"
                min-length="3"
                className='form-control'
                id="cvv"
                required
                value={state.cvv}
                onChange={handleInputData}
                onBlur={handleBlur}
                placeholder="CVV"
                placeholderChar={'\u2000'}
              />
              
            </div>
            {error && error.expiryError && error.expiryError.length > 1 && (
                <span className='form__form-group-error'>{error.expiryError}</span>
            )}
            {error && error.securityCodeError && error.securityCodeError.length > 1 && (
                <span className='form__form-group-error'>{error.securityCodeError}</span>
              )}
          </div>
          <div className="my-3">
            <label className="form__form-group-label" htmlFor="name">
              Name on Card
            </label>
            <input
              type="text"
              id="name"
              name="name"
              required
              placeholder="ADEBAYO ABDULMALIK ADEOLA"
            />
          </div>
          <div className="my-3">
            <label className="form__form-group-label" htmlFor="country">
              Country or region
            </label>
            <Select
              type="text"
              className="react-select__input"
              id="country"
              name="country"
              required
              options={countriesOptions}
              defaultValue={countriesOptions[0]}
            />
          </div>
          <div className="my-3">
            <button type="submit" className="btn btn-primary btn-block " onClick={handleSubmit}>
              Pay
            </button>
            <div className="mt-3 text-center">
              <img
                className="align-middle d-inline-block"
                src={padlock}
                alt="Padlock"
                width="20px"
                height="20px"
              />{' '}
              <span className="align-middle ms-2">Secured By MiraPay</span>
            </div>
          </div>
        </form>
      </div>
    </section>
  );
};

// Export Component
export default CheckoutForm;
