// Import the necessary libraries and modules.
import React, { useState } from 'react';
import CheckoutForm from './checkoutForm';


// Checkout Form
const Checkout = () => {
    const [cardList, setCardList] = useState([]);

    return (
        <CheckoutForm setCardList={setCardList} cardList={cardList} />
    );
}


// Export component
export default Checkout;